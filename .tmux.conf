# Setting the prefix from `C-b` to `C-a`.
# By remapping the `CapsLock` key to `Ctrl`,
# you can make triggering commands more comfottable!
set -g prefix C-a

# Free the original `Ctrl-b` prefix keybinding.
unbind C-b

set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator'

# Nord Theme
# https://www.nordtheme.com/docs/ports/tmux/installation
set -g @plugin "arcticicestudio/nord-tmux"


# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'


# More lines to scroll
set -g history-limit 10000

# Ensure that we can send `Ctrl-a` to other apps.
bind C-a send-prefix

# Reload the file with Prefix r.
bind r source-file ~/.tmux.conf \; display "Reloaded!"

# Splitting panes.
bind v split-window -h -c '#{pane_current_path}'
bind s split-window -v -c '#{pane_current_path}'

# Moving between panes.
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Moveing between windows.
# Provided you've mapped your `CAPS LOCK` key to the `CTRL` key,
# you can now move between panes without moving your hands off the home row.
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# Pane resizing.
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

# Maximize and restore a pane.
unbind Up
bind Up new-window -d -n tmp \; swap-pane -s tmp.1 \; select-window -t tmp
unbind Down
bind Down last-window \; swap-pane -s tmp.1 \; kill-window -t tmp

# Log output to a text file on demand.
bind P pipe-pane -o "cat >>~/#W.log" \; display "Toggled logging to ~/#W.log"

# Vimlike copy mode.
unbind [
bind Escape copy-mode
unbind p
bind p paste-buffer

# Setting the delay between prefix and command.
set -sg escape-time 1

# Set the base index for windows to 1 instead of 0.
set -g base-index 1

# Set the base index for panes to 1 instead of 0.
setw -g pane-base-index 1

# Mouse support - set to on if you want to use the mouse.
set -g mouse on
setw -g mouse on

# Enable activity alerts.
setw -g monitor-activity on
set -g visual-activity on

set -g pane-border-lines double

# Enable vi keys.
setw -g mode-keys vi

# Status bar
set -g status-left-length 52
set -g status-right-length 451

# Add truecolor support
set-option -ga terminal-overrides ",xterm-256color:Tc"


# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
set -g status-right "#(/bin/bash $HOME/.tmux/kube.tmux 250 red cyan)"
run -b '~/.tmux/plugins/tpm/tpm'
