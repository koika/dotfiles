" Pluginmanager needs to be istalled
" https://github.com/junegunn/vim-plug
"
" To install execute
" curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs " https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

" Plugins {{{
call plug#begin('~/.config/nvim/plugged')

Plug 'fatih/vim-go' "🔥
Plug 'rust-lang/rust.vim'
Plug 'leafgarland/typescript-vim'
Plug 'tpope/vim-fugitive' "🔥
Plug 'tpope/vim-commentary'
Plug 'SirVer/ultisnips'
Plug 'Raimondi/delimitMate'
Plug 'ekalinin/Dockerfile.vim', {'for' : 'Dockerfile'}
Plug 'tveskag/nvim-blame-line'
Plug 'jamessan/vim-gnupg'
Plug 'posva/vim-vue'
Plug 'hashivim/vim-terraform'
" Plug 'mattn/webapi-vim'
Plug 'cespare/vim-toml'
Plug 'jparise/vim-graphql'
" Plug 'christoomey/vim-tmux-navigator'

" Fuzzy search stuff
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Markdown preview
" Plug 'previm/previm'
" Plug 'tyru/open-browser.vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

" coc
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Visual
Plug 'arcticicestudio/nord-vim'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/goyo.vim'
Plug 'dracula/vim'

" Bazel
Plug 'google/vim-maktaba'
Plug 'bazelbuild/vim-bazel'

call plug#end()
" }}}

set guifont=SF\ Mono:h24


" Autocommands Filetypes {{{
if has("autocmd")
  filetype plugin indent on " Enable file type detection

  autocmd FileType make       setlocal ts=4 sts=4 sw=4 noexpandtab nolist
  autocmd FileType yaml       setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType cucumber   setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType ruby       setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType proto      setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType scala      setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType python     setlocal ts=4 sts=4 sw=4 expandtab   list
  autocmd FileType scss       setlocal ts=3 sts=2 sw=2 expandtab   list
  autocmd FileType ps1        setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType sh         setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType c          setlocal ts=4 sts=4 sw=4 noexpandtab nolist
  autocmd FileType cpp        setlocal ts=4 sts=4 sw=4 expandtab   list
  autocmd FileType coffee     setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType less       setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType vim        setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType html       setlocal ts=4 sts=4 sw=4 noexpandtab nolist
  autocmd FileType erb        setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType css        setlocal ts=4 sts=4 sw=4 noexpandtab nolist
  autocmd FileType javascript setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType json       setlocal ts=2 sts=2 sw=2 expandtab   list
  autocmd FileType snippet    setlocal ts=8 sts=8 sw=8 noexpandtab nolist
  autocmd FileType xml        setlocal ts=4 sts=4 sw=4 expandtab   list
  autocmd FileType d          setlocal ts=4 sts=4 sw=4 expandtab   list
  autocmd FileType gohtmltmpl setlocal ts=4 sts=4 sw=4 noexpandtab nolist
  autocmd FileType markdown   setlocal nonumber spell
  autocmd FileType man        setlocal nonumber showbreak=""

  " Treat .rss files as XML
  autocmd BufNewFile,BufRead *.rss setfiletype xml

  " Gemfile is also ruby
  autocmd BufNewFile,BufRead Gemfile setfiletype ruby

  autocmd TermOpen * setlocal nonumber
endif


" }}}

" General {{{
set nocompatible    " explicitly get out of vi-compatible mode
set nobackup                   " do not make backup files
set nowritebackup
set fileformats=unix,dos,mac   " support all three, in this order
set hidden                     " you can change buffers without saving
set maxmempattern=10000

set iskeyword+=_,$,@,%,#       " none of these are word dividers
                               " This could break something

set noerrorbells               " don't make noise
set directory=$HOME/.vim/swap// " change folder for swp and swo files

" }}}

" Git Blame Line {{{
nnoremap <silent> <leader>b :ToggleBlameLine<CR>
"autocmd BufEnter * EnableBlameLine " Use autocmd to enable on startup
" }}}

" UX Interactive {{{
set breakindent                " indent long wrapped lines
set backspace=indent,eol,start " make backspace a more flexible
set clipboard=unnamedplus      " share clipboard with X11/wayland
set mouse=a                    " enable mouse control
let mapleader=","              " Set leader key
set incsearch                  " do search and highlight while typing
set inccommand=nosplit         " provide live feedback on substitutions
set laststatus=2               " always show the status line
"set lazyredraw                 " do not redraw while running macros
set nostartofline              " leave my cursor where it was
set novisualbell               " don't blink
set report=0                   " always show the last :.. command
set shortmess=aOstTc           " shortens messages to avoid
set statusline+=%f
                               " 'press a key' prompt
"set foldmethod=indent
"set foldnestmax=1

map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-l> <C-W>l
map <C-h> <C-W>h

noremap <Up> gk
noremap <Down> gj
noremap j gj
noremap k gk

set grepprg=pt

noremap <silent> <C-S-N> :cn<CR>
noremap <silent> <C-S-P> :cp<CR>
" location list
noremap <C-S-n> :lnext<CR>
noremap <C-S-p> :lprevious<CR>


" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

set cpoptions=aABceFsmq
             "|||||||||
             "||||||||`-- When joining lines, leave the cursor
             "||||||||    between joined lines
             "|||||||`--- When a new match is created (showmatch)
             "|||||||     pause for .5
             "||||||`---- Set buffer options when entering the
             "||||||      buffer
             "|||||`----- :write command updates current file name
             "||||`------ Automatically add <CR> to the last line
             "||||        when using :@r
             "|||`------- Searching continues at the end of the match
             "|||         at the cursor position
             "||`-------- A backslash has no special meaning in mappings
             "|`--------- :write updates alternative file name
             "`---------- :read updates alternative file name
set whichwrap=b,s,h,l,<,>,~,[,] " everything wraps
            " | | | | | | | | |
            " | | | | | | | | `-- "]" Insert and Replace
            " | | | | | | | `---- "[" Insert and Replace
            " | | | | | | `------ "~" Normal
            " | | | | | `-------- <Right> Normal and Visual
            " | | | | `---------- <Left> Normal and Visual
            " | | | `------------ "l" Normal and Visual (not recommended)
            " | | `-------------- "h" Normal and Visual (not recommended)
            " | `---------------- <Space> Normal and Visual
            " `------------------ <BS> Normal and Visual
set wildmenu " turn on command line completion wild style

" ignore these list file extensions
set wildignore=*.dll,*.o,*.obj,*.bak,*.exe,*.pyc,
\*.jpg,*.gif,*.png

set wildmode=list:longest " turn on wild mode huge list

" Unflold with doubleclick
" nnoremap <expr> <2-LeftMouse> foldclosed(line('.')) == -1 ? "\<2-LeftMouse>" : 'zo'

" }}}

" UI Appeareance {{{
set number
set termguicolors
if exists('+termguicolors')
  let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
colorscheme nord
syntax on
hi VertSplit ctermbg=NONE guibg=NONE

set listchars=tab:»·,trail:· " show tabs and trailing
set matchtime=5              " how many tenths of a second to blink
set fillchars+=vert:│        " Improve vertical split lines
set fillchars+=fold:-        " Improve vertical split lines
set sidescrolloff=10         " Keep 5 lines at the size
set showcmd                  " show the command being typed
set showmatch                " show matching brackets
set ruler                    " show current positions along the bottom
set scrolloff=4              " keep the cursor vertical middle if possible
set showbreak=↳\             " Icon to indicate wrapped lines
set linebreak                " Only linkebreak whole words
" }}}

" Text Formatting/Layout {{{
set formatoptions=rq " Automatically insert comment leader on return,
                     " and let gq format comments
set ignorecase       " case insensitive by default
set infercase        " case inferred by default
set shiftround       " when at 3 spaces, and I hit > ... go to 4, not 5
set smartcase        " if there are caps, go case-sensitive
" }}}

" markdown-preview {{{

" set to 1, nvim will open the preview window after entering the markdown buffer
" default: 0
let g:mkdp_auto_start = 0

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
" leave from insert mode, default 0 is auto refresh markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 0

" set to 1, the MarkdownPreview command can be use for all files,
" by default it can be use in markdown file
" default: 0
let g:mkdp_command_for_global = 0

" set to 1, preview server available to others in your network
" by default, the server listens on localhost (127.0.0.1)
" default: 0
let g:mkdp_open_to_the_world = 0

" use custom IP to open preview page
" useful when you work in remote vim and preview on local browser
" more detail see: https://github.com/iamcco/markdown-preview.nvim/pull/9
" default empty
let g:mkdp_open_ip = ''

" specify browser to open preview page
" default: ''
let g:mkdp_browser = ''

" set to 1, echo preview page url in command line when open preview page
" default is 0
let g:mkdp_echo_preview_url = 0

" a custom vim function name to open preview page
" this function will receive url as param
" default is empty
let g:mkdp_browserfunc = ''

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
" content_editable: if enable content editable for preview page, default: v:false
" disable_filename: if disable filename header for preview page, default: 0
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {},
    \ 'content_editable': v:false,
    \ 'disable_filename': 0
    \ }

" use a custom markdown style must be absolute path
" like '/Users/username/markdown.css' or expand('~/markdown.css')
let g:mkdp_markdown_css = ''

" use a custom highlight style must absolute path
" like '/Users/username/highlight.css' or expand('~/highlight.css')
let g:mkdp_highlight_css = ''

" use a custom port to start server or random for empty
let g:mkdp_port = ''

" preview page title
" ${name} will be replace with the file name
let g:mkdp_page_title = '「${name}」'

" recognized filetypes
" these filetypes will have MarkdownPreview... commands
let g:mkdp_filetypes = ['markdown']
" }}}


" Coc {{{
let g:coc_global_extensions = [
 \'coc-css',
 \'coc-emoji',
 \'coc-eslint',
 \'coc-explorer',
 \'coc-json',
 \'coc-marketplace',
 \'coc-prettier',
 \'coc-pyright',
 \'coc-rust-analyzer',
 \'coc-tsserver',
 \'coc-java',
 \'coc-clangd',
 \'coc-yaml']


nmap <silent> <2-LeftMouse> <Plug>(coc-definition)

 " \'coc-rls',
" File Explorer
nmap <silent> <Leader>t :CocCommand explorer<CR>

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)


" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
" Note coc#float#scroll works on neovim >= 0.4.0 or vim >= 8.2.0750
nnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
nnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
inoremap <nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"

" NeoVim-only mapping for visual mode scroll
" Useful on signatureHelp after jump placeholder of snippet expansion
if has('nvim')
  vnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#nvim_scroll(1, 1) : "\<C-f>"
  vnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#nvim_scroll(0, 1) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
" }}}}

" Rust {{{
let g:rustfmt_autosave = 1
" }}}

" Go {{{
let g:go_fmt_command = "goimports"
let g:go_def_mapping_enabled = 0
let g:go_fmt_fail_silently = 0
let g:go_term_enabled = 1
let g:go_highlight_array_whitespace_error = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_chan_whitespace_error = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_space_tab_error = 1
let g:go_highlight_string_spellcheck = 1
let g:go_highlight_structs = 1
let g:go_highlight_trailing_whitespace_error = 1
"}}}

" Lightline {{{
" let g:lightline = {'colorscheme': 'wombat' }
let g:lightline = {'colorscheme': 'nord' }
" }}}

" Text Formatting/Layout {{{
set formatoptions=rq " Automatically insert comment leader on return,
                     " and let gq format comments
set ignorecase       " case insensitive by default
set infercase        " case inferred by default
set shiftround       " when at 3 spaces, and I hit > ... go to 4, not 5
set smartcase        " if there are caps, go case-sensitive
" }}}

" Close Hidden buffers {{{
command! CloseHiddenBuffers call s:CloseHiddenBuffers()
function! s:CloseHiddenBuffers()
  let open_buffers = []

  for i in range(tabpagenr('$'))
    call extend(open_buffers, tabpagebuflist(i + 1))
  endfor

  for num in range(1, bufnr("$") + 1)
    if buflisted(num) && index(open_buffers, num) == -1
      exec "bdelete ".num
    endif
  endfor
endfunction
map <F10> :CloseHiddenBuffers<CR>
"}}}

" Previm {{{
" let g:previm_open_cmd = 'previm-openbrowser'
" }}}

" Custom Shortcuts {{{
  nnoremap <silent> <leader><tab> :Rg<CR>
  nnoremap <silent> <leader>fm :Marks<CR>
  nnoremap <silent> <leader>fb :Buffers<CR>
" }}}

set encoding=utf8
" vim:foldmethod=marker:foldlevel=0
